import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { makeStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import RadioButtonsGroup from "./radioGroups";
import QuestionsAddedText from "./questionTextAdded";

import { evaluationsUser } from '../../../../../src/services/database/evaluationsUser';

const useStyles = makeStyles({
    width: {
        width: '50%'
    },
    padding: {
        padding: '10px'
    }
});


export default function FormDialog({ openDialog, dialogData, closeDialog, dbUsers, actualizeData, openMode, dbEvaluations }) {
    const classes = useStyles();
    const { authUser } = useSelector(({ auth }) => auth);
    const [open, setOpen] = React.useState(false);
    const [data, setData] = React.useState(null);
    const [user, setUser] = React.useState(0);
    const [qualification, setQualification] = React.useState('');
    const [users, setUsers] = React.useState([]);
    const [evaluations, setEvaluations] = React.useState([]);
    const [evaluation, setEvaluation] = React.useState(0);
    const [mode, setMode] = React.useState(0);

    useEffect(() => {
        setUsers(dbUsers); 
        setEvaluations(dbEvaluations);
        setOpen(openDialog);
        if (dialogData) {
            setData(dialogData);
            setUser(dialogData.evaluated_user_id);
        }

        setMode(openMode);
    }, [openDialog, dialogData]);

    const handleClose = () => {
        setOpen(false);
        closeDialog();
    };

    const handleChangeUser = (e) => {
        setUser(e.target.value)
    }

    const handleChangeEvaluation = (e) => {
        setEvaluation(e.target.value);
        const found = evaluations.find( data => data.id === e.target.value );
        setData(found);
    }

    const handleQualification = (e) => {
        setQualification(e.target.value)
    }

    const sendQualification = async () => {
        const { id: evaluationUserId } = data;
        const { id: userId } = authUser;
        const response = await evaluationsUser.sendQualificationToUser(evaluationUserId, qualification, userId);
        console.log('RESPONSE ', response)
        const { data: dataResponse } = response;
        actualizeData(dataResponse);
    }

    const assignUser = async () => {
        const { evaluation_id, id: evaluationUserId = 0 } = data; 
        const evaluationId = (evaluation_id) ? evaluation_id : evaluation;
        const { id: userId } = authUser;
        const response = await evaluationsUser.assignEvaluationToUser(userId, user, evaluationId, mode, evaluationUserId);
        const { data: dataResponse } = response;
        actualizeData(dataResponse);
    }

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth={true}
                maxWidth={'sm'}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Questions and answers</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {data && data.evaluationName}
                    </DialogContentText>

                    <FormControl className={classes.width} >
                        <InputLabel id="demo-simple-select-label">Select user</InputLabel>
                        <Select
                            disabled={(data && data.presented) || ''}
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={user}
                            onChange={handleChangeUser}
                        >
                            {(users && users.length > 0) &&
                                users.map((item, i) => {
                                    return <MenuItem value={item.id}> {item.name} </MenuItem>
                                })
                            }
                        </Select>
                    </FormControl>
                    <FormControl className={classes.width} >
                        <InputLabel id="demo-simple-select-label">Select evaluation</InputLabel>
                        <Select
                            disabled={(data && data.presented) || ''}
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={evaluation}
                            onChange={handleChangeEvaluation}
                        >
                            {(evaluations && evaluations.length > 0) &&
                                evaluations.map((item, i) => {
                                    return <MenuItem value={item.id}> {item.name} </MenuItem>
                                })
                            }
                        </Select>
                    </FormControl>
                    <br></br><br></br><br></br><br></br>
                    {data &&
                        (<><div>
                            {
                                data.questions && data.questions.map((item, i) => {
                                    if(item.type === 1){
                                        return (<RadioButtonsGroup questions={item} />)
                                    }else{
                                        return (<QuestionsAddedText questions={item} />)
                                    }
                                   
                                })
                            }
                        </div>
                            {data.presented &&
                                <TextField
                                    value={qualification}
                                    autoFocus
                                    margin="dense"
                                    label="Qualification"
                                    type="email"
                                    className={classes.width}
                                    fullWidth
                                    onChange={handleQualification}
                                />
                            }</>)
                    }
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>

                    {data && data.presented ?
                        <Button onClick={sendQualification} color="primary">Send qualification</Button>
                        :
                        <Button onClick={assignUser} color="primary">Assign</Button>
                    }

                </DialogActions>
            </Dialog>
        </div>
    );
}