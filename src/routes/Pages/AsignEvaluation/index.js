import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import Box from '@material-ui/core/Box';
import IntlMessages from '../../../@jumbo/utils/IntlMessages';
import Grid from '@material-ui/core/Grid';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

// Dialog.
import FormDialog from "./customComponents/modal";

// Services.
import { evaluationsUser } from '../../../../src/services/database/evaluationsUser';

const breadcrumbs = [
  { label: 'Home', link: '/' },
  { label: 'Evaluations', isActive: true },
];

const useStyles = makeStyles({
  table: {
    minWidth: 650
  }
});


const AsignEvaluations = () => {
  const classes = useStyles();
  const { authUser } = useSelector(({ auth }) => auth);

  const [evaluationsData, setEvaluations] = useState([]);
  const [evaluations, setDbEvaluations] = useState([]);
  const [openDialog, setOpenDialog] = useState(false);
  const [dialogData, setDialogData] = useState({});
  const [users, setUsers] = useState({});
  const [mode, setMode] = React.useState(0);

  useEffect(() => {

    async function getEvaluationsUser() {
      const { id: userId } = authUser;
      const response = await evaluationsUser.getEvaluationsByEvaluatorUser(userId);
      const { data, users, evaluations } = response;
      setEvaluations(data);
      setDbEvaluations(evaluations);
      setUsers(users);
    }

    getEvaluationsUser();
  }, []);

  const handleOpenDialog = (e, row) => {
    e.preventDefault();
    setOpenDialog(true);
    setDialogData(row);
    setMode(1);
  };

  const handleOpenDialogCreate = (e) => {
    e.preventDefault();
    setOpenDialog(true);
    setMode(0);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
    setDialogData({});
    setMode(0)
  };

  const actualizeData = (data) => {
    setEvaluations(data);
    setOpenDialog(false);
    setDialogData({});
  }

  return (
    <>
      <PageContainer heading={<IntlMessages id="pages.evaluations" />} breadcrumbs={breadcrumbs}>
        <GridContainer>
          <Grid item xs={12}>
            <Box>
              <IntlMessages id="pages.evaluations.description" />
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box>
              <Button onClick={handleOpenDialogCreate} color="primary">
                Asign Evaluation
              </Button>
            </Box>
          </Grid>

          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Date</TableCell>
                  <TableCell align="right">Title</TableCell>
                  <TableCell align="right">Score</TableCell>
                  <TableCell align="right">User score</TableCell>
                  <TableCell align="right">Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {evaluationsData.map(row => (
                  <TableRow key={row.evaluationDate}>
                    <TableCell component="th" scope="row">
                      {row.evaluationDate}
                    </TableCell>
                    <TableCell align="right">{row.evaluationName}</TableCell>
                    <TableCell align="right">{row.evaluationScore}</TableCell>
                    <TableCell align="right">{row.userScore}</TableCell>
                    <TableCell align="right">
                      <a href="#" onClick={(e) => handleOpenDialog(e, row)}><VisibilityIcon /></a>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </GridContainer>

      </PageContainer>
      <FormDialog
        openDialog={openDialog}
        dialogData={dialogData}
        closeDialog={handleCloseDialog}
        dbUsers={users}
        actualizeData={actualizeData}
        openMode={mode}
        dbEvaluations={evaluations}
      />
    </>
  );
};

export default AsignEvaluations;
