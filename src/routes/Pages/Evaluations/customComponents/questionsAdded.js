import React, { useEffect, forwardRef, useImperativeHandle } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import CheckIcon from '@material-ui/icons/Check';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import EditIcon from '@material-ui/icons/Edit';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles({
    fieldText: {
        width: '100%'
    },
    areaA: {
        marginLeft: '30px'
    },
    addOption: {
        height: '30px'
    },
});

const QuestionsAdded = (props, ref) => {
    useImperativeHandle(
        ref,
        () => ({
            doneAll() {
                const { nSelected } = props;
                const dataObject = { nSelected, questionObject, options };
                return dataObject;
            }
        }),
    )

    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    let [options, setOptions] = React.useState([]);
    const [questionObject, setQuestionObject] = React.useState({});

    useEffect(() => {
        setQuestionObject(props.question);

    }, [props.question]);

    const handleChange = (event) => {
        const { value } = event.target;
        setValue(value);

        setOptions(
            options.map((item, n) =>
                item.option === value
                    ? { ...item, isCorrect: true }
                    : { ...item, isCorrect: false }
            ));

        let tempOptions = options;
        tempOptions.map((item, n) => {
            if (item.option === value) {
                item.isCorrect = true
            } else {
                item.isCorrect = false;
            }
        });

        const { checkQuestion, nSelected } = props;
        checkQuestion(nSelected, questionObject, tempOptions);

    };

    const handleNameQuestion = (event) => {
        const { name, value } = event.target;
        setQuestionObject(prevState => ({
            ...prevState,
            question: value
        }));
    }

    const addOptions = (e) => {
        e.preventDefault();
        const option = {
            option: ``,
            isCorrect: false,
            edited: 0,
            n: options.length
        };

        setOptions([...options, option])
    }

    const handleNameOption = (e, i) => {
        setOptions(
            options.map((item, n) =>
                n === i
                    ? { ...item, option: e.target.value }
                    : item
            ));
    }

    const handleCheckQuestion = (e, val) => {
        e.preventDefault();
        setQuestionObject(prevState => ({
            ...prevState,
            edited: val,
        }));

        const { checkQuestion, nSelected } = props;
        checkQuestion(nSelected, questionObject, options);
    }

    const handleCheckOption = (e, n, val) => {
        e.preventDefault();
        setOptions(
            options.map((item, i) =>
                i === n
                    ? { ...item, edited: val }
                    : item
            ));

        const { checkQuestion, nSelected } = props;
        checkQuestion(nSelected, questionObject, options);
    }

    return (
        <FormControl component="fieldset" ref={ref}>

            <div className=''>
                {questionObject.edited === 0 ?
                    <>
                        <TextField
                            value={questionObject.question}
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Write a question"
                            type="text"
                            className={`${classes.fieldText}`}
                            onChange={handleNameQuestion}
                        />
                        <a href="#" onClick={(e) => handleCheckQuestion(e, 1)} > <CheckIcon /></a>

                    </>
                    : <>
                        <FormLabel component="legend" className={`${classes.fieldText}`}>{questionObject.question}</FormLabel>
                        <a href="#" onClick={(e) => handleCheckQuestion(e, 0)} className={`col-lg-1`} ><EditIcon /></a>

                    </>
                }
                
                <Button className={classes.addOption} onClick={addOptions} ><AddIcon />Add option</Button>

            </div>

            <RadioGroup className={classes.areaA} aria-label="answers" name={questionObject.question} value={value} onChange={handleChange}>
                {options &&
                    options.map((item, i) => {
                        return (
                            item.edited === 0 ?
                                <>
                                    <TextField
                                        value={item.option}
                                        autoFocus
                                        margin="dense"
                                        id="name"
                                        label="Write an option"
                                        type="text"
                                        className={`${classes.fieldText}`}
                                        onChange={(e) => handleNameOption(e, i)}
                                    />
                                    <a href="#" onClick={(e) => handleCheckOption(e, i, 1)} > <CheckIcon /></a>

                                </>
                                : <>
                                    <FormControlLabel
                                        value={item.option}
                                        control={<Radio />}
                                        label={item.option}
                                    />

                                    <a href="#" onClick={(e) => handleCheckOption(e, i, 0)} ><EditIcon /></a>
                                </>
                        )

                    })
                }
            </RadioGroup>
        </FormControl >
    );

}


export default React.forwardRef(QuestionsAdded);