import React, { useEffect, useRef } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';
import CheckIcon from '@material-ui/icons/Check';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import AddIcon from '@material-ui/icons/Add';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import RadioButtonsGroup from "./radioGroups";
import QuestionsAdded from "./questionsAdded";
import QuestionsAddedText from "./questionTextAdded";


// Services.
import { evaluations } from '../../../../../src/services/database/evaluationsUser';

const useStyles = makeStyles({
    fieldText: {
        width: '100%'
    },
    areaA: {
        marginLeft: '30px'
    },
    addOption: {
        height: '30px'
    },
});


export default function FormDialogCreate({ openDialog, closeDialog, actualizeData }) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [data, setData] = React.useState({});
    const [addedQuestions, setAddedQuestions] = React.useState([]);
    const [editName, setEditName] = React.useState(0);
    const [evaluationName, setEvaluationName] = React.useState('Write an evaluation');
    const [questionType, setQuestionType] = React.useState('1');

    const questionsAdded = useRef(null);

    useEffect(() => {
        setOpen(openDialog);
        // setData(dialogData);
        // console.log('DIALOG DATA ',dialogData)
    }, [openDialog]);

    const handleChangeType = (e) => {
        setQuestionType(e.target.value);
    }

    const handleClose = () => {
        // setData({});
        setOpen(false);
        closeDialog();
    };

    const addQuestion = (e) => {
        e.preventDefault();
        const { id } = data;
        let object = {
            evaluation_id: id,
            question: '',
            type: questionType,
            answers: [],
            edited: 0,
        }

        setAddedQuestions([...addedQuestions, object])
    }

    const checkQuestion = (ns, question, options) => {
        let tempArrObjects = addedQuestions;

        tempArrObjects[ns] = question;
        tempArrObjects[ns].answers = options;
        setAddedQuestions(tempArrObjects);

        console.log('QUESTIONS OPTIONS ', tempArrObjects)
    }

    const checkQuestionText = (ns, question) => {
        let tempArrObjects = addedQuestions;

        tempArrObjects[ns].question = question;
        tempArrObjects[ns].answers = [];
        setAddedQuestions(tempArrObjects);

        console.log('QUESTIONS OPTIONS TEXT', tempArrObjects)
    }

    const handleEditName = (e, value) => {
        e.preventDefault();
        setEditName(value);
    }

    const handleSetEvaluationName = (e) => {
        setEvaluationName(e.target.value);
    }

    const save = async () => {
        console.log('QUESTIONSSS ', addedQuestions)
        
        setAddedQuestions(addedQuestions);
        console.log('TEMP ARR OBJECTSS ', addedQuestions)

        const { id: evaluationId } = data;
        const response = await evaluations.saveDataEvaluation(evaluationId, addedQuestions, evaluationName); 
        const { actualizedData } = response;
        actualizeData(actualizedData);
    }

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth={true}
                maxWidth={'sm'}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Questions and answers</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        <div>
                            <TextField
                                value={evaluationName}
                                autoFocus
                                margin="dense"
                                id="name"
                                label="Write a name"
                                type="text"
                                className={`${classes.fieldText}`}
                                onChange={(e) => handleSetEvaluationName(e)}
                            />
                        </div>
                    </DialogContentText>

                    <Button onClick={addQuestion}> <AddIcon /> Add question</Button>
                    <RadioGroup row={true} className={classes.areaA} aria-label="answers" value={questionType} onChange={handleChangeType}>
                        <FormControlLabel
                            value={'1'}
                            control={<Radio />}
                            label='Selection'
                        />
                        <FormControlLabel
                            value={'2'}
                            control={<Radio />}
                            label='Text'
                        />
                    </RadioGroup>
                    <br></br>
                    <div className={classes.areaA}>
                        {
                            addedQuestions && addedQuestions.map((item, i) => {
                                if (item.type === '1') {
                                    return (<><QuestionsAdded ref={questionsAdded} question={item} nSelected={i} checkQuestion={checkQuestion} /><br></br></>)
                                } else {
                                    return (<><QuestionsAddedText question={item} nSelected={i} checkQuestionText={checkQuestionText} /><br></br></>)
                                }
                            })

                        }
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
          </Button>
                    <Button onClick={save} color="primary">
                        Send
          </Button>
                </DialogActions>
            </Dialog>
        </div >
    );
}